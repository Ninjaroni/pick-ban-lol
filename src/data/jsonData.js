let data = {
    "teams": [
        {
            "name": "Kliktech",
            "players": [],
            "matches": [
                {
                    "date": "09-04-2018",
                    "picks": [
                        {"name": "fiora"},{"name": "zac"},{"name": "ryze"},{"name": "ashe"},{"name": "karma"}
                    ],
                    "bans": [
                        {"name": "kaisa"},{"name": "camile"},{"name": "sion"},{"name": "skarner"},{"name": "olaf"}
                    ],
                    "enemy": "could be better",
                    "victory": true
                },
                {
                    "date": "10-04-2018",
                    "picks": [
                        {"name": "camile"},{"name": "zac"},{"name": "galio"},{"name": "cait"},{"name": "rakan"}
                    ],
                    "bans": [
                        {"name": "xayah"},{"name": "kaisa"},{"name": "morg"},{"name": "fiora"},{"name": "thresh"}
                    ],
                    "enemy": "big",
                    "victory": true
                },
                {
                    "date": "11-04-2018",
                    "picks": [
                        {"name": "cho"},{"name": "skarner"},{"name": "leblanc"},{"name": "xayah"},{"name": "morg"}
                    ],
                    "bans": [
                        {"name": "camile"},{"name": "ryze"},{"name": "swain"},{"name": "galio"},{"name": "orianna"}
                    ],
                    "enemy": "big",
                    "victory": true
                },
                {
                    "date": "12-04-2018",
                    "picks": [
                        {"name": "kled"},{"name": "trundle"},{"name": "ryze"},{"name": "cait"},{"name": "taric"}
                    ],
                    "bans": [
                        {"name": "kha"},{"name": "nida"},{"name": "morg"},{"name": "swain"},{"name": "skarner"}
                    ],
                    "enemy": "K1ck",
                    "victory": true
                },
                {
                    "date": "12-04-2018",
                    "picks": [
                        {"name": "shen"},{"name": "olaf"},{"name": "sion"},{"name": "xayah"},{"name": "karma"}
                    ],
                    "bans": [
                        {"name": "vlad"},{"name": "orn"},{"name": "ryze"},{"name": "swain"},{"name": "skarner"}
                    ],
                    "enemy": "K1ck",
                    "victory": true
                },
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "gang"},{"name": "kha"},{"name": "taliyah"},{"name": "xayah"},{"name": "rakan"}
                    ],
                    "bans": [
                        {"name": "cassio"},{"name": "morg"},{"name": "cait"},{"name": "swain"},{"name": "kaisa"}
                    ],
                    "enemy": "euronics",
                    "victory": true
                },
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "kled"},{"name": "trundle"},{"name": "sion"},{"name": "cait"},{"name": "morg"}
                    ],
                    "bans": [
                        {"name": "gang"},{"name": "anivia"},{"name": "braum"},{"name": "shen"},{"name": "orn"}
                    ],
                    "enemy": "origen",
                    "victory": true
                },
                {
                    "date": "17-04-2018",
                    "picks": [
                        {"name": "camile"},{"name": "seju"},{"name": "ryze"},{"name": "kaisa"},{"name": "tahm"}
                    ],
                    "bans": [
                        {"name": "anivia"},{"name": "kha"},{"name": "cait"},{"name": "fiora"},{"name": "orn"}
                    ],
                    "enemy": "excel",
                    "victory": true
                }
            ]
        },
        {
            "name": "Movistar Riders",
            "players": [],
            "matches": [
                {
                    "date": "09-04-2018",
                    "picks": [
                        {"name": "renekton"},{"name": "lee sin"},{"name": "orianna"},{"name": "kaisa"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "swain"},{"name": "rakan"},{"name": "skarner"},{"name": "morgana"},{"name": "gangplank"}
                    ],
                    "enemy": "Forge",
                    "victory": true
                },
                {
                    "date": "10-04-2018",
                    "picks": [],
                    "bans": [],
                    "enemy": "PPK",
                    "victory": true
                },
                {
                    "date": "11-04-2018",
                    "picks": [
                        {"name": "yasuo"},{"name": "jarvan"},{"name": "ori"},{"name": "trist"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "jax"},{"name": "ez"},{"name": "camile"},{"name": "swain"},{"name": "shyvana"}
                    ],
                    "enemy": "Big",
                    "victory": true
                },
                {
                    "date": "12-04-2018",
                    "picks": [
                        {"name": "ryze"},{"name": "skarner"},{"name": "azir"},{"name": "ez"},{"name": "shen"}
                    ],
                    "bans": [
                        {"name": "camile"},{"name": "orn"},{"name": "velkoz"},{"name": "swain"},{"name": "nida"}
                    ],
                    "enemy": "LDLC",
                    "victory": true
                },
                {
                    "date": "12-04-2018",
                    "picks": [
                        {"name": "ryze"},{"name": "lee"},{"name": "azir"},{"name": "trist"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "nida"},{"name": "camile"},{"name": "xayah"},{"name": "kha"},{"name": "shen"}
                    ],
                    "enemy": "LDLC",
                    "victory": true
                },
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "kennen"},{"name": "trundle"},{"name": "kass"},{"name": "cait"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "tahm"},{"name": "vlad"},{"name": "kha"},{"name": "galio"},{"name": "swain"}
                    ],
                    "enemy": "mad lions",
                    "victory": false
                },
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "shen"},{"name": "kha"},{"name": "ryze"},{"name": "jihn"},{"name": "morg"}
                    ],
                    "bans": [
                        {"name": "camile"},{"name": "varus"},{"name": "xayah"},{"name": "kled"},{"name": "morg"}
                    ],
                    "enemy": "nip",
                    "victory": false
                },
                {
                    "date": "17-04-2018",
                    "picks": [],
                    "bans": [],
                    "enemy": "SPGE",
                    "victory": true
                }
            ]
        },
        {
            "name": "Excel",
            "players": [],
            "matches":[
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "shen"},{"name": "trundle"},{"name": "karma"},{"name": "kaisa"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "ryze"},{"name": "morg"},{"name": "xayah"},{"name": "kha"},{"name": "skarner"}
                    ],
                    "enemy": "euronics",
                    "victory": true
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "cassio"},{"name": "seju"},{"name": "karma"},{"name": "kaisa"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "renek"},{"name": "morg"},{"name": "camile"},{"name": "anivia"},{"name": "swain"}
                    ],
                    "enemy": "origen",
                    "victory": false
                },
                {
                    "date": "17-04-2018",
                    "picks": [
                        {"name": "cho"},{"name": "skarner"},{"name": "galio"},{"name": "varus"},{"name": "morg"}
                    ],
                    "bans": [
                        {"name": "tahm"},{"name": "cassio"},{"name": "cait"},{"name": "ryze"},{"name": "swain"}
                    ],
                    "enemy": "kliktech",
                    "victory": true
                }
            ]
        },
        {
            "name": "Origen",
            "players": [],
            "matches":[
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "cho"},{"name": "kha"},{"name": "xayah"},{"name": "trist"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "cassio"},{"name": "ryze"},{"name": "swain"},{"name": "camile"},{"name": "xayah"}
                    ],
                    "enemy": "kliktech",
                    "victory": false
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "sion"},{"name": "skarner"},{"name": "ori"},{"name": "trist"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "xayah"},{"name": "cait"},{"name": "trundle"},{"name": "shen"},{"name": "gang"}
                    ],
                    "enemy": "excel",
                    "victory": true
                },
                {
                    "date": "17-04-2018",
                    "picks": [
                        {"name": "cho"},{"name": "olaf"},{"name": "cassio"},{"name": "trist"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "zilean"},{"name": "janna"},{"name": "trundle"},{"name": "swain"},{"name": "xayah"}
                    ],
                    "enemy": "euronics",
                    "victory": true
                }
            ]
        },
        {
            "name": "Millenium",
            "players": [],
            "matches":[
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "cho"},{"name": "seju"},{"name": "cassio"},{"name": "trist"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "ori"},{"name": "syndra"},{"name": "ryze"},{"name": "kaisa"},{"name": "azir"}
                    ],
                    "enemy": "misfits",
                    "victory": true
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "malph"},{"name": "seju"},{"name": "cassio"},{"name": "trist"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "kha"},{"name": "tahm"},{"name": "ryze"},{"name": "cait"},{"name": "swain"}
                    ],
                    "enemy": "atlantis",
                    "victory": true
                },
                {
                    "date": "17-04-2018",
                    "picks": [
                        {"name": "shen"},{"name": "reksai"},{"name": "kass"},{"name": "kaisa"},{"name": "morg"}
                    ],
                    "bans": [
                        {"name": "ryze"},{"name": "ali"},{"name": "cait"},{"name": "camile"},{"name": "aatrox"}
                    ],
                    "enemy": "war",
                    "victory": true
                }
            ]
        },
        {
            "name": "Team Atlantis",
            "players": [],
            "matches":[
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "sion"},{"name": "kha"},{"name": "zilean"},{"name": "varus"},{"name": "taric"}
                    ],
                    "bans": [
                        {"name": "xayah"},{"name": "skarner"},{"name": "kaisa"},{"name": "zoe"},{"name": "ryze"}
                    ],
                    "enemy": "wind and rain",
                    "victory": true
                },
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "mao"},{"name": "kha"},{"name": "ryze"},{"name": "sivir"},{"name": "taric"}
                    ],
                    "bans": [
                        {"name": "skarner"},{"name": "kaisa"},{"name": "cassio"},{"name": "azir"},{"name": "orn"}
                    ],
                    "enemy": "misfits",
                    "victory": false
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "camile"},{"name": "trundle"},{"name": "galio"},{"name": "varus"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "xayah"},{"name": "skarner"},{"name": "kaisa"},{"name": "jax"},{"name": "orn"}
                    ],
                    "enemy": "millenium",
                    "victory": false
                }
            ]
        },
        {
            "name": "Ad Hoc Gaming",
            "players": [],
            "matches":[
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "cho"},{"name": "skarner"},{"name": "ori"},{"name": "kaisa"},{"name": "tahm"}
                    ],
                    "bans": [
                        {"name": "trist"},{"name": "cait"},{"name": "trundle"},{"name": "swain"},{"name": "sion"}
                    ],
                    "enemy": "illuminar",
                    "victory": false
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "gnar"},{"name": "olaf"},{"name": "taliyah"},{"name": "xayah"},{"name": "rakan"}
                    ],
                    "bans": [
                        {"name": "orn"},{"name": "azir"},{"name": "kaisa"},{"name": "nidalee"},{"name": "swain"}
                    ],
                    "enemy": "gamersorigin",
                    "victory": false
                },
                {
                    "date": "17-04-2018",
                    "picks": [],
                    "bans": [],
                    "enemy": "penguins",
                    "victory": false
                }
            ]
        },
        {
            "name": "Wind and Rain",
            "players": [],
            "matches":[
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "aatrox"},{"name": "trundle"},{"name": "anivia"},{"name": "jihn"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "azir"},{"name": "cassio"},{"name": "cait"},{"name": "olaf"},{"name": "swain"}
                    ],
                    "enemy": "team atlantis",
                    "victory": false
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "sion"},{"name": "nunu"},{"name": "cassio"},{"name": "xayah"},{"name": "zilean"}
                    ],
                    "bans": [
                        {"name": "trundle"},{"name": "camile"},{"name": "olaf"},{"name": "orn"},{"name": "cho"}
                    ],
                    "enemy": "misfits",
                    "victory": false
                },
                {
                    "date": "17-04-2018",
                    "picks": [
                        {"name": "illaoi"},{"name": "trundle"},{"name": "talon"},{"name": "xayah"},{"name": "rakan"}
                    ],
                    "bans": [
                        {"name": "orn"},{"name": "kha"},{"name": "sion"},{"name": "swain"},{"name": "cassio"}
                    ],
                    "enemy": "millenium",
                    "victory": false
                }
            ]
        },
        {
            "name": "SPG Esports",
            "players": [],
            "matches":[
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "sion"},{"name": "trundle"},{"name": "xerath"},{"name": "varus"},{"name": "morg"}
                    ],
                    "bans": [
                        {"name": "jax"},{"name": "camile"},{"name": "galio"},{"name": "kha"},{"name": "seju"}
                    ],
                    "enemy": "mad lions",
                    "victory": false
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "orn"},{"name": "seju"},{"name": "anivia"},{"name": "trist"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "kled"},{"name": "camile"},{"name": "olaf"},{"name": "fiora"},{"name": "azir"}
                    ],
                    "enemy": "NIP",
                    "victory": true
                },
                {
                    "date": "17-04-2018",
                    "picks": [],
                    "bans": [],
                    "enemy": "movistar",
                    "victory": false
                }
            ]
        },
        {
            "name": "GamersOrigin",
            "players": [],
            "matches":[
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "camile"},{"name": "lee"},{"name": "ryze"},{"name": "xayah"},{"name": "rakan"}
                    ],
                    "bans": [
                        {"name": "ori"},{"name": "cassio"},{"name": "sion"},{"name": "swain"},{"name": "skarner"}
                    ],
                    "enemy": "penguins",
                    "victory": true
                },
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "camile"},{"name": "nida"},{"name": "xayah"},{"name": "ezreal"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "skarner"},{"name": "sion"},{"name": "cait"},{"name": "jax"},{"name": "vlad"}
                    ],
                    "enemy": "illuminar",
                    "victory": true
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "mao"},{"name": "graves"},{"name": "ryze"},{"name": "ezreal"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "skarner"},{"name": "sion"},{"name": "cait"},{"name": "camile"},{"name": "shen"}
                    ],
                    "enemy": "adhoc",
                    "victory": true
                }
            ]
        },
        {
            "name": "Misfits Academy",
            "players": [],
            "matches":[
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "sion"},{"name": "trundle"},{"name": "swain"},{"name": "ezreal"},{"name": "tahm"}
                    ],
                    "bans": [
                        {"name": "xayah"},{"name": "skarner"},{"name": "cait"},{"name": "kha"},{"name": "orn"}
                    ],
                    "enemy": "millenium",
                    "victory": false
                },
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "sion"},{"name": "trundle"},{"name": "syndra"},{"name": "varus"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "trist"},{"name": "ezreal"},{"name": "xayah"},{"name": "cait"},{"name": "swain"}
                    ],
                    "enemy": "atlantis",
                    "victory": true
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "shen"},{"name": "skarner"},{"name": "azir"},{"name": "varus"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "zac"},{"name": "seju"},{"name": "kaisa"},{"name": "cait"},{"name": "swain"}
                    ],
                    "enemy": "wind and rain",
                    "victory": true
                }
            ]
        },
        {
            "name": "Penguins",
            "players": [],
            "matches":[
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "orn"},{"name": "trundle"},{"name": "anivia"},{"name": "kaisa"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "graves"},{"name": "kha"},{"name": "nidalee"},{"name": "galio"},{"name": "renekton"}
                    ],
                    "enemy": "gamersorigin",
                    "victory": false
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "gnar"},{"name": "trundle"},{"name": "ekko"},{"name": "cait"},{"name": "taric"}
                    ],
                    "bans": [
                        {"name": "sion"},{"name": "alistar"},{"name": "braum"},{"name": "camile"},{"name": "vlad"}
                    ],
                    "enemy": "illuminar",
                    "victory": true
                },
                {
                    "date": "17-04-2018",
                    "picks": [],
                    "bans": [],
                    "enemy": "ad hoc",
                    "victory": true
                }
            ]
        },
        {
            "name": "NIP",
            "players": [],
            "matches":[
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "sion"},{"name": "olaf"},{"name": "cassio"},{"name": "cait"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "alistar"},{"name": "tahm"},{"name": "trundle"},{"name": "kaisa"},{"name": "swain"}
                    ],
                    "enemy": "movistar",
                    "victory": true
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "sion"},{"name": "trundle"},{"name": "kass"},{"name": "cait"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "xerath"},{"name": "varus"},{"name": "rakan"},{"name": "kaisa"},{"name": "swain"}
                    ],
                    "enemy": "SPGE",
                    "victory": false
                },
                {
                    "date": "17-04-2018",
                    "picks": [
                        {"name": "camile"},{"name": "ivern"},{"name": "ryze"},{"name": "trist"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "fiora"},{"name": "jax"},{"name": "tahm"},{"name": "kaisa"},{"name": "swain"}
                    ],
                    "enemy": "mad lions",
                    "victory": false
                }
            ]
        },
        {
            "name": "Euronics",
            "players": [
                {
                    "playerName": "tolkin",
                    "playerRegion": "euw1"
                },
                {
                    "playerName": "phrenic",
                    "playerRegion": "euw1"
                },
                {
                    "playerName": "zazee",
                    "playerRegion": "euw1"
                },
                {
                    "playerName": "conjo",
                    "playerRegion": "euw1"
                },
                {
                    "playerName": "anthrax",
                    "playerRegion": "euw1"
                }
            ],
            "matches":[
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "orn"},{"name": "trundle"},{"name": "syndra"},{"name": "sivir"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "zac"},{"name": "camile"},{"name": "ryze"},{"name": "galio"},{"name": "sion"}
                    ],
                    "enemy": "kliktech",
                    "victory": false
                },
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "orn"},{"name": "seju"},{"name": "azir"},{"name": "varus"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "vlad"},{"name": "gang"},{"name": "camile"},{"name": "swain"},{"name": "rakan"}
                    ],
                    "enemy": "excel",
                    "victory": false
                },
                {
                    "date": "17-04-2018",
                    "picks": [
                        {"name": "camile"},{"name": "seju"},{"name": "ryze"},{"name": "kaisa"},{"name": "tahm"}
                    ],
                    "bans": [
                        {"name": "anivia"},{"name": "kha"},{"name": "cait"},{"name": "fiora"},{"name": "orn"}
                    ],
                    "enemy": "origen",
                    "victory": false
                }
            ]
        },
        {
            "name": "Mad Lions",
            "players": [],
            "matches":[
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "jax"},{"name": "seju"},{"name": "azir"},{"name": "jihn"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "ryze"},{"name": "kaisa"},{"name": "rakan"},{"name": "karma"},{"name": "morg"}
                    ],
                    "enemy": "movistar",
                    "victory": true
                },
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "orn"},{"name": "skarner"},{"name": "azir"},{"name": "cait"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "gang"},{"name": "cho"},{"name": "kaisa"},{"name": "swain"},{"name": "rakan"}
                    ],
                    "enemy": "SPGE",
                    "victory": true
                },
                {
                    "date": "17-04-2018",
                    "picks": [
                        {"name": "sion"},{"name": "trundle"},{"name": "azir"},{"name": "exreal"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "kled"},{"name": "rakan"},{"name": "cait"},{"name": "seju"},{"name": "skarner"}
                    ],
                    "enemy": "NIP",
                    "victory": true
                }
            ]
        },
        {
            "name": "Illuminar",
            "players": [
                {
                    "playerName": "hello",
                    "playerRegion": "euw1"
                },
                {
                    "playerName": "hello",
                    "playerRegion": "euw1"
                },
                {
                    "playerName": "hello",
                    "playerRegion": "euw1"
                },
                {
                    "playerName": "hello",
                    "playerRegion": "euw1"
                },
                {
                    "playerName": "hello",
                    "playerRegion": "euw1"
                }
            ],
            "matches":[
                {
                    "date": "14-04-2018",
                    "picks": [
                        {"name": "vlad"},{"name": "olaf"},{"name": "cassio"},{"name": "ezreal"},{"name": "alistar"}
                    ],
                    "bans": [
                        {"name": "ryze"},{"name": "azir"},{"name": "nida"},{"name": "kennen"},{"name": "camile"}
                    ],
                    "enemy": "ad hoc",
                    "victory": true
                },
                {
                    "date": "15-04-2018",
                    "picks": [
                        {"name": "poppy"},{"name": "olaf"},{"name": "azir"},{"name": "trist"},{"name": "braum"}
                    ],
                    "bans": [
                        {"name": "renek"},{"name": "zoe"},{"name": "kaisa"},{"name": "ryze"},{"name": "swain"}
                    ],
                    "enemy": "gamersorigin",
                    "victory": false
                },
                {
                    "date": "16-04-2018",
                    "picks": [
                        {"name": "orn"},{"name": "kha"},{"name": "azir"},{"name": "ashe"},{"name": "tahm"}
                    ],
                    "bans": [
                        {"name": "taliyah"},{"name": "xerath"},{"name": "kaisa"},{"name": "ryze"},{"name": "swain"}
                    ],
                    "enemy": "penguins",
                    "victory": false
                }
            ]
        }
    ]
};

export default data;