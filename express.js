var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var cors = require('cors')
var path = require('path')
var http = require('http');
var https = require("https");

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.post('/api/find-player-id', function(req, res) {

    var playerRegion = req.body.playerRegion,
        playerName =req.body.playerName;

    //TODO: Beware of spaces in names! May need to encode.

    var options = {
        host: playerRegion + '.api.riotgames.com',
        path: '/lol/summoner/v3/summoners/by-name/' + playerName,
        method: 'GET',
        headers: {
            "X-Riot-Token": "RGAPI-c2658e92-e670-4372-9045-a8492a2c4d50"
        }
    };

    https.get(options, resData => {
        resData.setEncoding("utf8");
        let body = "";
        resData.on("data", data => {
            body += data;
        });
        resData.on("end", () => {
            body = JSON.parse(body);
            res.send(body);
        });
    });
});

app.post('/api/find-player-champs', function(req, res) {

    var playerRegion = req.body.playerRegion,
        playerId =req.body.playerId;

    //TODO: Beware of spaces in names! May need to encode.
console.log(playerRegion + '.api.riotgames.com' + '/lol/champion-mastery/v3/champion-masteries/by-summoner/' + playerId);
    var options = {
        host: playerRegion + '.api.riotgames.com',
        path: '/lol/champion-mastery/v3/champion-masteries/by-summoner/' + playerId,
        method: 'GET',
        headers: {
            "X-Riot-Token": "RGAPI-c2658e92-e670-4372-9045-a8492a2c4d50"
        }
    };

    https.get(options, resData => {
        resData.setEncoding("utf8");
        let body = "";
        resData.on("data", data => {
            body += data;
        });
        resData.on("end", () => {
            body = JSON.parse(body);
            res.send(body);
        });
    });
});

app.listen(3000);
